class CreateSlides < ActiveRecord::Migration[5.0]
  def change
    create_table :slides do |t|
      t.string :name
      t.text :desc
      t.text :author
      t.attachment :picture

      t.timestamps
    end
  end
end
